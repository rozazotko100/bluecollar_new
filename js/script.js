// слайдер
$('.slider').slick();


//3 секція, підписка на клік кнопки
const serviceButtons =$('button[data-services]');

serviceButtons.click(function (e){
    const service = $(this).data('services')

    $('.service[data-services]').removeClass('open')
    $(`.service[data-services="${service}"]`).addClass('open')

    serviceButtons.removeClass('button_white')
    $(this).addClass('button_white')

    })

// робимо плавні скроли
$('a[href^="#"]').on('click', function(event) {

    const target = $( $(this).attr('href') );

    if( target.length ) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: target.offset().top
        }, 2000);
    }

});
